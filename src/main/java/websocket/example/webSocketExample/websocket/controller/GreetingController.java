package websocket.example.webSocketExample.websocket.controller;

import lombok.SneakyThrows;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;
import websocket.example.webSocketExample.websocket.model.Greeting;
import websocket.example.webSocketExample.websocket.model.HelloMessage;

@Controller
public class GreetingController {

    @SneakyThrows
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting (HelloMessage message) {
        Thread.sleep(1000);
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }
}
